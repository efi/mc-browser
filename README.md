# mc-browser

A browser for Minecraft mods using the Twitch API.

## Installation

1) Install Clojure and Leiningen if necessary.

2) $ git clone

## Usage

$ ./run

 or

$ lein run

No binaries available yet.
Windows and Mac not built or tested yet.

This software is **unreleased** and changes will happen. Download at your own risk.
