(defproject curseforge-curses "0.1"
  :description "A minecraft mod browser"
  :url "http://git.lubar.me/efi/mc-browse"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/core.async "0.7.559"]
                 [seancorfield/next.jdbc "1.0.13"]
                 [org.xerial/sqlite-jdbc "3.28.0"]
                 [seesaw "1.5.0"]
                 [net.mikera/imagez "0.12.0"]
                 [org.clojure/data.json "0.2.7"]]
  :main ^:skip-aot mc-browser.core
  :target-path "target/%s"
  :resources-path "resources"
  :profiles {:uberjar {:aot :all}})
