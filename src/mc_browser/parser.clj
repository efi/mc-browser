(ns mc-browser.parser
  (:require [clojure.string :refer [trim replace]]
            [clojure.data.json :as json]))

(defn- log [o] (println o) o)

(def base-url "https://addons-ecs.forgesvc.net/api/v2/")
(def search "addon/search")
(def categories "category")
(def versions "minecraft/version")
(def mc-mods "?gameid=432&sectionid=6")
(def mod-data "addon/")
(def sortby "&sort=")
(defn mod-files [modid] (str "addon/" modid "/files"))

(defn url [uri] (java.net.URL. uri))

(defn get-json [uri]
  (with-open [r (clojure.java.io/reader (url uri))]
    (json/read r :key-fn keyword)))

(defn get-mod-data [n]
  (get-json (str base-url mod-data n)))
    
(defn get-mod-search [s]
  (get-json (str base-url search mc-mods "&searchfilter=" s)))

(defn get-categories []
  (get-json (str base-url categories mc-mods)))

(defn get-versions []
  (get-json (str base-url versions)))

(defn get-files [modid]
  (get-json (str base-url (mod-files modid))))
