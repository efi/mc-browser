(ns mc-browser.core
  (:require [next.jdbc :as jdbc]
            [next.jdbc.sql :as sql]
            [clojure.string :refer [join split]]
            [clojure.core.async :as async]
            [mc-browser.parser :as p]
            [seesaw.core :as ss]
            [seesaw.color :refer [color]]
            [seesaw.mig :as mig]
            [seesaw.border :as border]
            [seesaw.bind :as bind]
            [seesaw.graphics :as g]
            [mikera.image.core :as img])
  (:gen-class))

(defn- log [o] (println o) o)

(defn- flog [o] (clojure.pprint/pprint o (clojure.java.io/writer "output.log")) o)

(def dbname "mc-mods.sql")

(declare con)

(defn create-connection []
  (jdbc/get-connection
    (jdbc/get-datasource
      {:dbtype "sqlite" :dbname dbname})))

(defn create-table [table cols]
  (jdbc/execute! con
    [(str
       "CREATE TABLE IF NOT EXISTS "
       table
       (when-not (empty? cols)
         (str " ( id INTEGER PRIMARY KEY AUTOINCREMENT, "
              (join ", " cols)
              " ) ")))]))

(def unqualified {:builder-fn next.jdbc.result-set/as-unqualified-maps})

(defn url [uri] (java.net.URL. uri))

(defn db<-metaversions [files]
  (let [vs (sql/query con ["SELECT * FROM versions"] unqualified)]
    (sql/insert-multi!
      con :metaversions
      [:fid :gvid]
      (reduce into
        (for [f files :let [id (:id f)]]
          (vec (for [v (:v f)]
            [id (:gvid (some #(when (= (:name %) v) %) vs))])))))))

(defn db<-dependencies [files]
  (sql/insert-multi!
    con :dependencies
    [:fid :pid]
    (reduce into
      (for [f files :let [id (:id f)]]
        (vec (for [d (:dependencies f)]
          [id (:addonId d)]))))))

(declare db<-prj cache)

(defn db<-files [data]
  ;(flog data)
  (when-let [deps (reduce into (map :dependencies (remove #(empty? (:dependencies %)) (:files data))))]
    (db<-dependencies (:files data))
    (doseq [d deps]
      (when (empty? (sql/find-by-keys con :projects {:pid (:addonId d)} unqualified))
        (db<-prj (p/get-mod-data (:addonId d))))))
  (sql/insert-multi!
    con :files
    [:fid :dname :fname :link :release :size :fdate :pid]
    (mapv #(do
             (swap! (:fvs cache) conj {:id (:id %) :v (:gameVersion %)})
             [(:id %)
              (:displayName %)
              (:fileName %)
              (:downloadUrl %)
              (:releaseType %)
              (:fileLength %)
              (:fileDate %)
              (:project data)])
          (:files data))
    unqualified))

(def default-icon "resources/heart.png")

(defn cache-icon [uri]
  (if (empty? uri)
    (clojure.java.io/file default-icon)
    (let [u (url uri)
          fname (re-find #"[^/]+$" (.getPath u))]
      (if (.exists (clojure.java.io/file (str "cache/" fname)))
        (clojure.java.io/file (str "cache/" fname))
        (try
          (clojure.java.io/make-parents (str "cache/" fname))
          (-> u
              img/load-image
              (img/resize 32)
              (img/save (str "cache/" fname)))
          (clojure.java.io/file (str "cache/" fname))
          (catch javax.imageio.IIOException _
            (clojure.java.io/file default-icon)))))))

(defn db<-metacats [cats]
  (sql/insert-multi!
    con :metacats
    [:pid :catsid]
    (mapv #(vector
             (:projectId %)
             (:categoryId %))
          cats)))

(defn db<-mem [mem]
  (or
    (-> (sql/find-by-keys con :mems {:mid (:userId mem)} unqualified) first :id)
    (-> (sql/insert!
          con :mems
          {:mid  (:userId mem)
           :name (:name mem)}
          unqualified)
        last last)))

(defn db<-metamems [mems]
  (sql/insert-multi!
    con :metamems
    [:pid :memsid]
    (mapv #(vector
             (:projectId %)
             (db<-mem %))
          mems)))

(defn db<-prj [prj]
  ;(log (:name prj))
  (swap! (:cats cache) into (map #(select-keys % [:projectId :categoryId]) (:categories prj)))
  (swap! (:mems cache) into (:authors prj))
  (sql/insert!
    con :projects
    {:pid   (:id prj)
     :name  (:name prj)
     :slug  (:slug prj)
     :icon  (:thumbnailUrl (some #(when (:isDefault %) %) (:attachments prj)))
     :cdate (:dateCreated prj)
     :rdate (:dateReleased prj)
     :udate (:dateModified prj)})
  (async/thread
    (swap! (:files cache) conj
           {:project (:id prj)
            :files (mapv #(select-keys % [:id :displayName :fileName :downloadUrl :releaseType :fileLength :fileDate :dependencies :gameVersion])
                         (p/get-files (:id prj)))})))

(defn db<-cats []
  (sql/insert-multi!
    con :cats
    [:cid :name :slug :icon]
    (mapv #(vector
             (:id %)
             (:name %)
             (:slug %)
             (:avatarUrl %))
          (filter #(and (= 432 (:gameId %))
                        (= 6 (:rootGameCategoryId %)))
                  (p/get-categories)))))

(defn db<-versions []
  (sql/insert-multi!
    con :versions
    [:gvid :name]
    (mapv #(vector
             (:gameVersionId %)
             (:versionString %))
          (p/get-versions))))

(defn db->prj [id]
  (sql/get-by-id con :projects id))

(defn db->prjs [ids]
  (sql/find-by-keys con :projects (mapv #(hash-map :pid %) ids)))

(declare root tabs project-display)

(defn show-warning [s]
  (let [w (ss/select root [:#warn])
        l (ss/select w [:#warn-l])]
    (ss/text! l s)
    (ss/show! w)))

(def border-raised  (border/compound-border (javax.swing.border.BevelBorder. javax.swing.border.BevelBorder/RAISED)  1))
(def border-lowered (border/compound-border (javax.swing.border.BevelBorder. javax.swing.border.BevelBorder/LOWERED) 1))

(defn project-button [p]
  (ss/horizontal-panel
    :class :project-button
    :user-data (:pid p)
    :border border-raised
    :items [(let [img (atom nil)
                  icon (ss/label :size [32 :by 32])]
              (bind/bind
                img
                (bind/property icon :icon))
              (async/thread
                (reset! img (-> p :icon cache-icon)))
              icon)
            [5 :by 5]
            (:name p)
            :fill-h]
    :listen [:mouse-pressed   (fn [ev] (ss/config! ev :border border-lowered))
             :mouse-released  (fn [ev] (ss/config! (ss/select (ss/to-root ev) [:.project-button]) :border border-raised))
             :mouse-clicked   (fn [ev]
                                (let [default (ss/select (ss/to-root ev) [:#default])]
                                  (when (= -1 (.indexOfTab default (:name p)))
                                    (.addTab default (:name p) (project-display (:pid p)))
                                    (ss/selection! default (:name p))
                                    (let [sel (.getSelectedIndex default)]
                                      (.setTabComponentAt
                                        default sel
                                        (ss/horizontal-panel
                                          :background (color 0 0 0 0)
                                          :items [(:name p)
                                                  [5 :by 5]
                                                  (ss/button
                                                    :size [16 :by 14]
                                                    :font {:size 16}
                                                    :text "❎"
                                                    :border nil
                                                    :listen [:action (fn [_] (.removeTabAt default (.indexOfTab default (:name p))))])]))))))]))

(defn project-list
  ([] (project-list nil))
  ([q]
   (doto
     (-> (ss/vertical-panel
           :items (vec (for [i (sql/query con [(or q "SELECT * FROM projects LIMIT 10")] unqualified)]
                         (project-button i))))
         (ss/scrollable :vscroll :always))
     (-> (.getVerticalScrollBar) (.setUnitIncrement 20)))))

(def sorting {"Creation Date" "cdate"
              "Update Date"   "udate"
              "Name"          "name"
              "Popularity"    "name"; gotta figure out these oops
              "Downloads"     "name"})

(def date-format (java.text.SimpleDateFormat. "dd/MM/yyyy"))

(defn date->str [d]
  (.format date-format d))

(defn str->date [s]
  (try
    (.parse date-format s)
    (catch java.text.ParseException _ false)))

(defn epoch [date]; Database uses seconds, java.util.Date returns milliseconds
  (int (* 0.001 (.getTime date))))

(defn project-panel [data]
  (str "<html><div style=\"width: 100%; padding: 5px;\">"
       "<table><tr><td style=\"vertical-align: middle; height: 32px;\"><img width=32 src=\"file:///"
         (.getAbsolutePath
           (cache-icon
             (->> data
                  :attachments
                  (filter #(= (:isDefault %) true))
                  first
                  :url)))
       "\"/></td>"
       "<td style=\"vertical-align: middle;\">" (:name data) "</td>"
       "</tr></table><hr/>"
       (:summary data)
       "</div></html>"))

(defn project-display [p]
  (let [data (atom nil)
        display (ss/vertical-panel)]
    (bind/bind
      data
      (bind/property display :items))
    (async/thread
      (reset! data [(project-panel (p/get-mod-data p))]))
    display))

(def cache {:files  (atom [])
            :cats   (atom [])
            :mems   (atom [])
            :fvs    (atom [])})

(def db-available? (atom true))

(defn process-cache []
  ;(flog @(:files cache))
  (when-not (empty? @(:files cache))
    (doseq [f @(:files cache)]
      (db<-files f))
    (reset! (:files cache) []))
  (db<-metacats @(:cats cache))
  (reset! (:cats cache) [])
  (db<-metamems @(:mems cache))
  (reset! (:mems cache) [])
  (db<-metaversions @(:fvs cache))
  (reset! (:fvs cache) []))

(defn fetch-search [s]
  (async/thread
    (loop []
      (if (compare-and-set! db-available? true false)
        (do
          (dorun
            (map async/<!!
                 (for [prj (p/get-mod-search s)]
                   (db<-prj prj))))
          (loop []
            (process-cache)
            (when-not (every? #(empty? (deref (val %))) cache)
              (recur)))
          (compare-and-set! db-available? false true))
        (recur)))))

; DRAGONS AHEAD (the melt your face kind)
(defmacro j [v & body]
  `(join ~v (remove empty? (list ~@body))))

(defn search [data]
  ;(log data)
  (ss/selection! (ss/select root [:#default]) 0)
  (let [c+ (not (empty? (:cats+ data)))
        c- (not (empty? (:cats- data)))
        v+ (not (empty? (:vers+ data)))
        v- (not (empty? (:vers- data)))
        q (j " "
             "SELECT * FROM projects WHERE"
             (j " AND "
                (when (or c+ c-)
                  (j " "
                     (when c+
                       (j " "
                          "pid IN ("
                          "SELECT pid FROM metacats WHERE"
                          "catsid IN ("
                            "SELECT cid FROM cats WHERE"
                              "id IN ("
                                (join ", " (:cats+ data))
                              ")"
                          ")"))
                     (when (and c+ c-) "EXCEPT")
                     (when (and c- (not c+)) "pid NOT IN (")
                     (when c-
                       (j " "
                          "SELECT pid FROM metacats WHERE"
                          "catsid IN ("
                            "SELECT cid FROM cats WHERE"
                              "id IN ("
                                (join ", " (:cats- data))
                              ")"
                          ")"))
                     ")"))
                (when (or v+ v-)
                  (j " "
                     (when v+
                       (j " "
                          "pid IN ("
                          "SELECT pid FROM files WHERE"
                          "fid IN ("
                            "SELECT fid FROM metaversions WHERE"
                              "gvid IN ("
                                "SELECT gvid FROM versions WHERE"
                                  "id IN ("
                                    (join ", " (:vers+ data))
                                  ")"
                              ")"
                          ")"))
                     (when (and v+ v-) "EXCEPT")
                     (when (and v- (not v+)) "pid NOT IN (")
                     (when v-
                       (j " "
                          "SELECT pid FROM files WHERE"
                          "fid IN ("
                            "SELECT fid FROM metaversions WHERE"
                              "gvid IN ("
                                "SELECT gvid FROM versions WHERE"
                                  "id IN ("
                                    (join ", " (:vers- data))
                                  ")"
                              ")"
                          ")"))
                     ")"))
                (let [date-mode
                      (case (:date-mode data)
                        "Created Before"      "cdate <="
                        "Created After"       "cdate >="
                        "Released Before"     "rdate <="
                        "Released After"      "rdate >="
                        "Last Update Before"  "udate <="
                        "Last Update After"   "udate >=")
                      date (str->date
                             (join "/"
                                   (vals
                                     (update
                                       (select-keys data [:day :month :year])
                                       :month
                                       #(inc
                                          (.indexOf
                                            (remove empty? (-> (java.text.DateFormatSymbols.) .getMonths))
                                            %))))))]
                  (if date
                    (str date-mode " datetime(" (epoch date) ", 'unixepoch')")
                    (show-warning "Wrong date. Ignoring it.")))
                (when-not (empty? (:text-filter data))
                  (j " "
                     "(name LIKE"        (str "\"%" (:text-filter data) "%\"")
                       "OR slug LIKE"  (str "\"%" (:text-filter data) "%\"")
                     ")")))
             "ORDER BY" (sorting (:sorting data)) "DESC"
             "LIMIT 100")]
    ;(log q)
    (ss/config! (ss/select root [:#plist]) :items [(project-list q)])
    (fetch-search (str (:text-filter data) "&pageSize=100"))
    (let [p (atom nil)]
      (bind/bind
        p
        (bind/property (ss/select root [:#plist]) :items))
      (async/thread
        (loop []
          (if (compare-and-set! db-available? true false)
            (do
              (reset! p [(project-list q)])
              (compare-and-set! db-available? false true))
            (recur)))))))

(defn tri-state [c d n]
  (let [bg (ss/button-group)]
    (ss/flow-panel
      :hgap 0
      :items [(doto
                (ss/toggle :group bg :size [12 :by 20] :border 0 :margin 0 :class #{c :add} :user-data d
                           :paint (fn [_ g] (g/draw g (g/rounded-rect 2 0 9 20) (g/style :background (color :green 32)))))
                (.setFocusPainted false))
              (doto
                (ss/toggle :group bg :size [12 :by 20] :border 0 :margin 0 :selected? true)
                (.setFocusPainted false))
              (doto
                (ss/toggle :group bg :size [12 :by 20] :border 0 :margin 0 :class #{c :remove} :user-data d
                           :paint (fn [_ g] (g/draw g (g/rounded-rect 2 0 9 20) (g/style :background (color :red 32)))))
                (.setFocusPainted false))
              [5 :by 10]
              n])))

(defn search-panel []
  (doto
    (-> (mig/mig-panel
          :constraints ["wrap 2" "[left][right, fill]" "[min]"]
          :items [["Sorting:"] [(-> (ss/combobox :id :sorting
                                                 :model (keys sorting))
                                    (ss/selection! "Name"))]; should be Popularity on release
                  ["Category:" "wrap"]
                  [(mig/mig-panel
                     :constraints ["wrap 3, ins i" "0[]10" "0[min]0"]
                     :border (border/line-border :color (color :black 64))
                     :items (->> (sql/query con ["SELECT * FROM cats"] unqualified)
                                 (sort-by :name)
                                 (mapv #(vector (tri-state :cat (:id %) (:name %))))))
                   "span, growx"]
                  ["Version:" "wrap"]
                  [(mig/mig-panel
                     :constraints ["wrap 7, ins i" "0[]10" "0[min]0"]
                     :border (border/line-border :color (color :black 64))
                     :items (->> (sql/query con ["SELECT * FROM versions"] unqualified)
                                 (sort-by :name
                                          (fn [l r]
                                            (let [DOT #"\."
                                                  [lv rv] (map
                                                            (fn [v]
                                                              (map
                                                                (fn [n]
                                                                  (Integer/parseInt n))
                                                                (split v DOT)))
                                                            [l r])]
                                              (if (= (first lv) (first rv))
                                                (if (= (second lv) (second rv))
                                                  (> (last lv) (last rv))
                                                  (> (second lv) (second rv)))
                                                (> (first lv) (first rv))))))
                                 (mapv #(vector (tri-state :ver (:id %) (:name %))))))
                   "span, growx"]
                  ["Date filter:"] [(ss/combobox
                                      :id :date-mode
                                      :model ["Created Before"
                                              "Created After"
                                              "Released Before"
                                              "Released After"
                                              "Last Update Before"
                                              "Last Update After"])]
                  ["Date:"] [(mig/mig-panel
                               :constraints ["right" "" ""]
                               :items (let [date (doto (java.util.Calendar/getInstance)
                                                   (.setTime (java.util.Date.)))
                                            d (long (.get date java.util.Calendar/DAY_OF_MONTH))
                                            m (long (.get date java.util.Calendar/MONTH))
                                            y (long (.get date java.util.Calendar/YEAR))
                                            mnames (object-array (remove empty? (-> (java.text.DateFormatSymbols.) .getMonths)))]
                                        [["Day:"]   [(ss/spinner :id :day   :model (ss/spinner-model d :from 1 :to 31))]
                                         ["Month:"] [(ss/spinner :id :month :model (doto (javax.swing.SpinnerListModel. mnames)
                                                                                     (.setValue (nth mnames m))))]
                                         ["Year:"]  [(ss/spinner :id :year  :model (ss/spinner-model y))]]))]
                  ["Filter:"] [(ss/text :id :text-filter)]
                  [(ss/button
                     :text "Search"
                     :listen [:action (fn [ev]
                                        (search
                                          (reduce
                                            (fn [prev [k v]]
                                              (into prev {k (ss/value v)}))
                                            (let [f (fn [c]
                                                      (map ss/user-data
                                                        (filter #(ss/config % :selected?)
                                                          (ss/select (ss/to-root ev) [c]))))]
                                              {:cats+ (f :.cat.add)
                                               :cats- (f :.cat.remove)
                                               :vers+ (f :.ver.add)
                                               :vers- (f :.ver.remove)})
                                            (ss/group-by-id (ss/to-root ev)))))])
                   "span,center"]])
        (ss/scrollable))
    (-> (.getVerticalScrollBar) (.setUnitIncrement 20))))

(defn warn-thing []
  (let [p (ss/horizontal-panel
            :id :warn
            :background :yellow
            :items [(ss/label :id :warn-l)
                    :fill-h
                    (ss/button
                      :background :yellow
                      :text "OK"
                      :listen [:action (fn [_] (ss/hide! (ss/select root [:#warn])))])])]
    (ss/hide! p)
    p))

(defn default-pane []
  (def tabs (atom []))
  (let [tabp (ss/tabbed-panel
               :id :default
               :placement :top
               :overflow :scroll)]
    (bind/bind
      tabs
      (bind/property tabp :tabs))
    (reset! tabs [{:title "Projects"
                   :content (ss/vertical-panel :id :plist :items [(project-list)])}
                  {:title "Search"
                   :content (search-panel)}])
    (ss/vertical-panel
      :items [(warn-thing)
              tabp])))

(defn loop- []
  (def root
    (ss/frame :title "Minecraft Mod Explorer"
              :content (default-pane)
              :size [670 :by 500]
              :icon "heart.png"
              :listen [:window-closing
                       (fn [ev]
                         (log "Closing...")
                         (loop []
                           (if (compare-and-set! db-available? true false)
                             (do (.close con)
                                 (log "DB closed.")
                                 (ss/dispose! ev)
                                 (ss/dispose! (ss/all-frames))
                                 )
                             (recur))))]))
  (ss/show! root))

(defn first-run []
  (log "Generating db...")
  (create-table "projects"      ["pid INTEGER UNIQUE ON CONFLICT REPLACE" "name" "slug" "icon" "cdate INTEGER" "rdate INTEGER" "udate INTEGER"])
  (create-table "files"         ["fid INTEGER UNIQUE ON CONFLICT REPLACE" "dname" "fname" "link" "release" "size INTEGER" "fdate INTEGER" "pid INTEGER"])
  (create-table "metaversions"  ["fid INTEGER" "gvid INTEGER"])
  (create-table "versions"      ["gvid INTEGER UNIQUE ON CONFLICT IGNORE" "name"])
  (create-table "cats"          ["cid INTEGER UNIQUE ON CONFLICT IGNORE" "name" "slug" "icon"])
  (create-table "metacats"      ["pid INTEGER" "catsid INTEGER"])
  (create-table "mems"          ["mid INTEGER UNIQUE ON CONFLICT IGNORE" "name"])
  (create-table "metamems"      ["pid INTEGER" "memsid INTEGER"])
  (create-table "dependencies"  ["fid INTEGER" "pid INTEGER"])
  (db<-cats)
  (db<-versions)
  (fetch-search ""))

(defn run []
  (let [db-real? (.exists (clojure.java.io/file dbname))]
    (def con (create-connection))
    (when-not db-real?
      (time (first-run)))
    #_(fetch-search "&sort=2&pageSize=100")
    (loop-)))

(defn -main
  [& args]
  (run))
